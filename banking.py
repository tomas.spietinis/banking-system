import random
import sqlite3

class BankingSystem:
    def __init__(self):
        self.main_menu()

    def check_db(self, column, val, where_col="pin"):
        try:
            cur.execute(f"SELECT {column} FROM card WHERE {where_col} = {val};")
            conn.commit()
            cur_balance = cur.fetchone()
            return cur_balance
        except sqlite3.OperationalError:
            print("pin------sqlite3.OperationalError-------")
            self.main_menu()


    def lun_checksum(self, a):
        su = 0
        for i in str(a[0::2]):
            s = int(i) * 2
            if s > 9:
                s = s - 9
            su += s
        for j in str(a[1::2]):
            su += int(j)
        checksum = 0
        while su % 10 != 0:
            su += 1
            checksum += 1
        return checksum


    def create_account(self):
        cur.execute(f"SELECT id FROM card;")
        conn.commit()
        accounts = cur.fetchall()
        user_id = len(accounts) + 1
        random.seed(user_id)
        a = str(400000000000000 + random.randint(000000000, 999999999))
        user_card = a + str(self.lun_checksum(a))
        random.seed(user_id)
        user_pin = random.randint(1000, 9999)
        cur.execute(f"INSERT INTO card(id, number, pin) VALUES ({user_id}, {user_card}, {user_pin});")
        conn.commit()
        print("Your card has been created")
        print("Your card number:")
        print(user_card)
        print("Your card PIN:")
        print(user_pin)


    def check_pin(self, card):
        pin = int(input("Enter your PIN:"))
        try:
            cur.execute(f"SELECT * FROM card WHERE pin = {pin} and number = {card};")
            conn.commit()
            accounts = cur.fetchone()
            if accounts:
                print("You have successfully logged in!")
                self.login_menu(pin)
            else:
                print("Wrong card number or PIN!")
                self.main_menu()
        except sqlite3.OperationalError:
            print("pin------sqlite3.OperationalError-------")
            self.main_menu()


    def check_login(self):
        card = input("Enter your card number:")
        if self.check_db("number", card, "number"):
            self.check_pin(card)
        else:
            print("Wrong card number or PIN!")
            self.main_menu()


    def balance(self, pin):
        my_balance = self.check_db("balance", pin)
        print("Balance:", my_balance[0])
        self.login_menu(pin)


    def add_income(self, pin):
        income = int(input("Enter income:"))
        cur_balance = self.check_db("balance", pin)
        cur.execute(f"UPDATE card SET balance = {income + int(cur_balance[0])} WHERE pin = {pin} ;")
        conn.commit()
        print("Income was added!")
        self.login_menu(pin)


    def do_transfer(self, pin):
        transfer_to = input("Transfer\nEnter card number:")
        if self.lun_checksum(transfer_to) % 10 == 0:
            transfer_to_balance = self.check_db("balance", transfer_to, "number")
            if transfer_to_balance:
                if self.check_db("number", pin) != self.check_db("number", transfer_to, "number"):
                    amount = int(input("Enter how much money you want to transfer:"))
                    your_balance = self.check_db("balance", pin)
                    if your_balance[0] >= amount:
                        cur.execute(
                            f"UPDATE card SET balance = {amount + int(transfer_to_balance[0])} WHERE number = {transfer_to};")
                        conn.commit()
                        cur.execute(
                            f"UPDATE card SET balance = {int(your_balance[0]) - amount} WHERE pin = {pin};")
                        conn.commit()
                        self.login_menu(pin)
                    else:
                        print("Not enough money!")
                        self.login_menu(pin)
                else:
                    print("ou can't transfer money to the same account!")
                    self.login_menu(pin)
            else:
                print("Such a card does not exist.")
                self.login_menu(pin)
        else:
            print("Probably you made a mistake in the card number. Please try again!")
            self.login_menu(pin)


    def close_account(self, pin):
        cur.execute(f"DELETE FROM card WHERE pin = {pin};")
        conn.commit()
        print("The account has been closed!")
        self.main_menu()


    def login_menu(self, pin):
        user_input = int(input("""
                1. Balance
                2. Add income
                3. Do transfer
                4. Close account
                5. Log out
                0. Exit \n"""))
        if user_input == 1:
            self.balance(pin)
        elif user_input == 2:
            self.add_income(pin)
        elif user_input == 3:
            self.do_transfer(pin)
        elif user_input == 4:
            self.close_account(pin)
        elif user_input == 5:
            print("You have successfully logged out!")
            self.main_menu()
        elif user_input == 0:
            exit("Bye!")
        else:
            self.main_menu()


    def main_menu(self):
        user_input = int(input("""
            1. Create an account
            2. Log into account
            0. Exit \n"""))
        if user_input == 1:
            self.create_account()
        elif user_input == 2:
            self.check_login()
        elif user_input == 0:
            exit("Bye!")
        else:
            self.main_menu()


conn = sqlite3.connect('card.s3db')
cur = conn.cursor()
query = "CREATE TABLE IF NOT EXISTS card(id INTEGER, number TEXT, pin TEXT, balance INTEGER DEFAULT 0);"
cur.execute(query)
conn.commit()
bank = BankingSystem()
while True:
    bank.main_menu()

# 4000001442725099
# 3201
# 4000009267565821
# 1926
